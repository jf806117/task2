from parameters3 import *
from taskE2 import *
import numpy as np
import matplotlib.pyplot as plt
from numpy import zeros, linspace


#Creation of a time axis 
dt = 178
t_end = 60*60*24*30
nt = int(t_end/dt)
t = linspace(0, t_end, nt)

#Array for prognostic variables 
Eta = np.zeros((nx,ny))
U = np.zeros((nx+1,ny))
V = np.zeros((nx,ny+1))
energies=np.zeros(nt)
x_U = np.linspace(0, L, nx+1)
y_V = np.linspace(0, L, ny+1)
x_middle = np.linspace(dx/2, L-dx/2, nx)

#Forward Backward/ Matsuno method
for n in range(0,nt):
    if n%2 == 0:
        for i in range(nx):
            for j in range(ny):
                 Eta[i,j]=Eta[i,j]-H*dt*((U[i+1,j]-U[i,j])/dx+(V[i,j+1]-V[i,j])/dy)
        for i in range(1,nx):
            for j in range(ny):
                V_on_U = (V[i,j]+V[i,j+1]+V[i-1,j]+V[i-1,j+1])/4
                U[i,j]=(U[i,j]+(f0+beta*y[j])*dt*V_on_U-g*dt*((Eta[i,j]-Eta[i-1,j])/dx)
                         -gamma*dt*U[i,j]+(tow_0*-np.cos((np.pi*y[j])/L)*dt)/(rho*H))
        for i in range(nx):
            for j in range(1,ny):
                U_on_V = (U[i,j]+U[i+1,j]+U[i,j-1]+U[i+1,j-1])/4
                V[i,j]=(V[i,j]-(f0+beta*y[j])*dt*U_on_V-g*dt*((Eta[i,j]-
                Eta[i,j-1])/dy)-gamma*dt*V[i,j])
                                 
    else:
         for i in range(nx):
            for j in range(ny):
                 Eta[i,j]=Eta[i,j]-H*dt*((U[i+1,j]-U[i,j])/dx+(V[i,j+1]-V[i,j])/dy)

         for i in range(nx):
            for j in range(1,ny):
                U_on_V = (U[i,j]+U[i+1,j]+U[i,j-1]+U[i+1,j-1])/4
                V[i,j]=(V[i,j]-(f0+beta*y[j])*dt*U_on_V-g*dt*((Eta[i,j]-
                Eta[i,j-1])/dy)-gamma*dt*V[i,j])

         for i in range(1,nx):
             for j in range(ny):
                 V_on_U = (V[i,j]+V[i,j+1]+V[i-1,j]+V[i-1,j+1])/4
                 U[i,j]=(U[i,j]+(f0+beta*y[j])*dt*V_on_U-g*dt*((Eta[i,j]-Eta[i-1,j])/dx)
                         -gamma*dt*U[i,j]+(tow_0*-np.cos((np.pi*y[j])/L)*dt)/(rho*H))
    energies[n]=(Energy(U,V,Eta,L,nx,ny))
   


#Make grids the same dimension
U_mid = (U[0:-1,:]+U[1:,:])/2
V_mid = (V[:,0:-1]+V[:,1:])/2

#Analytically calculate the errors
U_error = U_mid - u_st
V_error = V_mid - v_st
Eta_error = Eta - eta_st

print(U_error, V_error, Eta_error)   

# #Calculate error in energy perturbation
E_error = (1/2)*rho*(H*(U_error**2+V_error**2)+g*Eta_error[i,j]**2)
I_error = E_error*dy*dx
WholeE = np.sum(I_error)
print('Error in energy perturbation over 30 days', E_error, 'J')
print('Total summation of error in energy perturbation', WholeE, 'J')

    
#Plot the figures
fig = plt.figure()
plt.plot(x_U,U[:,0])
plt.xlabel('Southern Edge of Basin')
plt.ylabel('Zonal Velocity (m/s)')
plt.title('Zonal Velocity close to Southern Edge of Basin')
plt.show()

fig = plt.figure()
plt.plot(y_V, V[0,:])
plt.xlabel('Western Edge of Basin')
plt.ylabel('Meridional Velocity close to Western Edge of Basin (m/s)')
plt.title('Meridional Velocity of Western Boundary Current')
plt.show()

fig = plt.figure()
plt.plot(x_middle, Eta[:,int(ny/2)])
plt.title('Surface Elevation through the middle of the gyre')
plt.xlabel('Zonal Component of the Basin')
plt.ylabel('Surface Elevation (Meters)')
plt.show()

SEplot = plt.contourf(x,y,Eta)
plt.colorbar(SEplot)
plt.xlabel('Southern Edge of the Basin')
plt.ylabel('Western Edge of the Basin')
plt.title('Surface Elevation over the Basin (Metres)')
plt.show()

fig = plt.figure()
plt.plot(t,energies)
plt.ylabel('Energy (PJs)')
plt.xlabel('Time')
plt.title('Energy of the perturbation from the resting ocean over time')
plt.show()


#Plot the errors
SEerror=plt.contourf(x,y,Eta_error)
plt.colorbar(SEerror)
plt.xlabel('Southern Edge of the Basin (meters)')
plt.ylabel('Western Edge of the Basin (meters)')
plt.title('Error of Surface Elevation (meters')
plt.show()

fig = plt.figure()
plt.plot(U_error[:,0])
plt.xlabel('Southern Edge of Basin (nx meters)')
plt.ylabel('Zonal Velocity Error (m/s)')
plt.title('Error in Zonal Velocity of Western Boundary Current')
plt.show()
 
fig = plt.figure()
plt.plot(V_error[0,:])
plt.xlabel('Western Edge of the basin, (ny meters)')
plt.ylabel('Meridional Velocity Error (m/s)')
plt.title('Error in Meridional Velocity of Western Boundary Current')
plt.show()




