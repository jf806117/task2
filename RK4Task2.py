from parameters3 import *
from Matsuno import *
import numpy as np
import matplotlib.pyplot as plt
from numpy import zeros, linspace

# Creation of a time axis
dt = 178
t_end = 60*60*24*30
nt = int(t_end/dt)
t = linspace(0, t_end, nt)

# Arrays for new variables
Eta_RK4 = np.zeros((nx, ny))
U_RK4 = np.zeros((nx+1, ny))
V_RK4 = np.zeros((nx, ny+1))

U[i+1,j]=U1
U[i,j] = U0
V[i,j] = V0
V[i,j+1] = V1
y[j] = y0
Eta[i,j] = Eta0
Eta[i,j-1] = Eta1

    
def evaluate(U1,U0, V1, V0, y0, V_on_U, U_on_V, Eta0,Eta1):
    print(U1,U0, V1, V0, y0, V_on_U, U_on_V, Eta0,Eta1)
    Eta_out = -H*((U1-U0)/dx+(V1-V0)/dy)
    U_out = ((f0+beta*y0)*V_on_U-g*((Eta0-Eta1)/dx)
             - gamma*U0+(tow_0*-np.cos((np.pi*y0)/L))/(rho*H))
    V_out = (-(f0+beta*y0)*U_on_V-g*((Eta0 - Eta1)/dy)-gamma*V0)

    return (U_out, V_out, Eta_out)


for n in range(0,nt):
    for i in range(nx):
        for j in range(ny):
            (Eta_RK4, U_RK4, V_RK4) = evaluate(Eta_RK4, U_RK4, V_RK4)
            k2 = evaluate(Eta_RK4*dt/2 + Eta_RK4, U_RK4*dt/2
                          + U_RK4, V_RK4*dt/2 + V_RK4)
            k3 = evaluate(k2[0]*dt/2+Eta_RK4, k2[1] *
                          dt/2 + U_RK4, k2[2]*dt/2+V_RK4)
            k4 = evaluate(k3[0]*dt + Eta_RK4, k3[1]*dt+U_RK4,
                          k3[2]*dt + V_RK4)

            Eta_RK4 = Eta_RK4+(dt/6)*(Eta_RK4+2*(k2[0]+k3[0]+k4[0]))
            U_RK4 = U_RK4+(dt/6)*(U_RK4+2*(k2[1]+k3[1]+k4[1]))
            V_RK4 = V_RK4+(dt/6)*(V_RK4+2*(k2[2]+k3[2]+k4[2]))


# Plot the figure
fig = plt.figure()
plt.plot(x_U, U_RK4[:, 0])
plt.xlabel('Southern Edge of Basin')
plt.ylabel('Zonal Velocity (m/s)')
plt.title('Zonal Velocity close to Southern Edge of Basin')
plt.show()

fig = plt.figure()
plt.plot(y_V, V_RK4[0, :])
plt.xlabel('Western Edge of Basin')
plt.ylabel('Meridional Velocity close to Western Edge of Basin (m/s)')
plt.title('Meridional Velocity of Western Boundary Current')
plt.show()

fig = plt.figure()
plt.plot(x_middle, Eta_RK4[:, int(ny/2)])
plt.title('Surface Elevation through the middle of the gyre')
plt.xlabel('Zonal Component of the Basin')
plt.ylabel('Surface Elevation (Meters)')
plt.show()

SEplot = plt.contourf(x, y, Eta_RK4)
plt.colorbar(SEplot)
plt.xlabel('Southern Edge of the Basin')
plt.ylabel('Western Edge of the Basin')
plt.title('Surface Elevation over the Basin (Metres)')
plt.show()

fig = plt.figure()
plt.plot(t, energies_RK4)
plt.ylabel('Energy')
plt.xlabel('Time')
plt.title('Energy of the perturbation from the resting ocean over time')
plt.show()

# Make grids the same dimension
U_midRK4 = (U[0:-1, :]+U[1:, :])/2
V_midRK4 = (V[:, 0:-1]+V[:, 1:])/2

# Analytically calculate the errors
U_errorRK4 = U_midRK4 - u_st
V_errorRK4 = V_midRK4 - v_st
Eta_errorRK4 = Eta_RK4 - eta_st

print(U_errorRK4, V_errorRK4, Eta_errorRK4)

# Plot the errors
SEerror = plt.contourf(x, y, Eta_errorRK4)
plt.colorbar(SEerror)
plt.xlabel('Southern Edge of the Basin (meters)')
plt.ylabel('Western Edge of the Basin (meters)')
plt.title('Error of Surface Elevation (meters')
plt.show()

fig = plt.figure()
plt.plot(U_errorRK4[:, 0])
plt.xlabel('Southern Edge of Basin (nx meters)')
plt.ylabel('Zonal Velocity Error (m/s)')
plt.title('Error in Zonal Velocity of Western Boundary Current')
plt.show()

fig = plt.figure()
plt.plot(V_errorRK4[0, :])
plt.xlabel('Western Edge of the basin, (ny meters)')
plt.ylabel('Meridional Velocity Error (m/s)')
plt.title('Error in Meridional Velocity of Western Boundary Current')
plt.show()
