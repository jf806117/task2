import numpy as np
import matplotlib.pyplot as plt

#Parameters
H = 1000  #resting depth of the fluid which is assumed constant (metres)
f0 = 1e-4  #Coriolis parameter approximation (/s)
beta = 1e-11 #Beta plane approximation 
g = 9.81   #gravitational acceleration (m/s^2)
gamma = 1e-6 #linear drag coefficient 
rho = 1000    #uniform denisty (kg/m^3)
L = 10**6      #Length scale of ocean basin(metres)
tow_0 = 0.2   #Inital wind stress (N/m^2)
Eta0 = 0      #Initial surface elevation
nx=41         #Number of grid divisions in x-direction
ny=41         #Number of grid divisions in y-direction
dx = 25000    #Grid spacing in x-direction
dy = 25000    #Grid spacing in y-direction


#Derived Parameters
epsilon = gamma/(beta*L)
a = (-1-np.sqrt(1+(2*np.pi*epsilon)**2))/(2*epsilon)
b = (-1+np.sqrt(1+(2*np.pi*epsilon)**2))/(2*epsilon)

def f1(x):
   return np.pi+ np.pi*((1+(np.exp(a)-1)*np.exp(b*x)+(1-np.exp(b))*np.exp(a*x))/
          (np.exp(b)- np.exp(a)))
           
def f2(x):
    return ((np.exp(a)-1)*b*np.exp(b*x)+(1-np.exp(b))*a*np.exp(a*x))/(np.exp(b) 
            -np.exp(a))
    
x = np.linspace(0,L,nx)
y= np.linspace(0,L,ny)   
X,Y = np.meshgrid(x,y)

u_st = (-tow_0/(np.pi*gamma*H*rho))*f1(X/L)*np.cos(np.pi*Y/L)
         
v_st = (tow_0/(np.pi*gamma*rho*H))*f2(X/L)*np.sin(np.pi*Y/L)

eta_st = (Eta0 + (tow_0/(np.pi*gamma*rho*H))*(f0*L/g)*(gamma/(f0*np.pi)*f2(X/L)*
np.cos(np.pi*Y/L)+(1/np.pi)*f1(X/L)*(np.sin(np.pi*Y/L))*(1+beta*Y/f0) + 
(beta*L)/(f0*np.pi)*np.cos(np.pi*Y/L)))

#Plot the analytical solutions
SEa = plt.contourf(X,Y,eta_st)
plt.colorbar(SEa)
plt.xlabel('Zonal Component (Meters)')
plt.ylabel('Meridional Component (Meters)')
plt.title('Analytical Solution Surface Elevation of North Atlantic Basin')
plt.show()
                                
Ua=plt.contourf(X,Y, u_st)
plt.colorbar(Ua)
plt.xlabel('Zonal Component (Meters)')
plt.ylabel('Meridional Component (Meters)')
plt.title('Analytical Solution of the Zonal Speed of the Western Boundary Current')
plt.show()

Va = plt.contourf(X,Y,v_st)
plt.colorbar(Va)
plt.xlabel('Zonal Component (Meters)')
plt.ylabel('Meridional Component (Meters)')
plt.title('Analytical Solution of the Meridional Speed of the Western Boundary Current')
plt.show()



                                                
        

